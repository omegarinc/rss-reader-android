package ru.ace.rssreader.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

import ru.ace.rssreader.rss.RssItem;

/**
 * Created by ACE on 31.01.2016.
 * <p/>
 * Database helper class to work with SQLite via OrmLite
 */
public class DatabaseHelper extends OrmLiteSqliteOpenHelper {
    // DB name
    private static final String DATABASE_NAME = "RssReaderDB.sqlite";

    // DB version
    private static final int DATABASE_VERSION = 1;

    // Classes added to DB
    private static final ClassPref[] CLASSES = new ClassPref[]{
            //Second parameter  - deleting table from DB when DB version was changed
            new ClassPref(RssItem.class, true)
    };

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db, ConnectionSource connection) {
        try {
            for (ClassPref classPref : CLASSES)
                TableUtils.createTableIfNotExists(connection, classPref.clazz);        //Creating tables for classes
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, ConnectionSource connection, int oldVersion, int newVersion) {
        try {
            for (ClassPref classPref : CLASSES) {
                if (classPref.dropOnUpgrade)
                    TableUtils.dropTable(connection, classPref.clazz, true);        // Delete table of class

                TableUtils.createTableIfNotExists(connection, classPref.clazz);        // Create table for class if not exists
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private static class ClassPref {
        Class<?> clazz;
        boolean dropOnUpgrade;

        public ClassPref(Class<?> clazz, boolean dropOnUpgrade) {
            this.clazz = clazz;
            this.dropOnUpgrade = dropOnUpgrade;
        }
    }

}
