package ru.ace.rssreader.db;

import android.content.Context;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.GenericRawResults;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;

import ru.ace.rssreader.RssReaderApp;
import ru.ace.rssreader.utils.ToolBox;

/**
 * Created by ACE on 31.01.2016.
 *
 * Manager to simple work with DB
 */
public enum DatabaseManager {

    INSTANCE;

    private DatabaseHelper databaseHelper;

    private DatabaseManager() {
        Context context = RssReaderApp.getAppContext();
        databaseHelper = new DatabaseHelper(context);
    }

    public <T> Dao<T, ?> getDao(Class<T> clazz) {
        try {
            return databaseHelper.getDao(clazz);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    // ////////////////////////////////////////////////////////////////////////////////////////////
    // ADDING METHODS
    // ////////////////////////////////////////////////////////////////////////////////////////////

    public <T> void create(final T data, Class<T> clazz) {
        if (data == null)
            return;

        try {
            final Dao<T, ?> dao = databaseHelper.getDao(clazz);
            dao.create(data);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public <T> void createOrUpdate(final T data, Class<T> clazz) {
        if (data == null)
            return;

        try {
            final Dao<T, ?> dao = databaseHelper.getDao(clazz);
            dao.createOrUpdate(data);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public <T> void createOrUpdate(final List<T> data, Class<T> clazz) {
        if (ToolBox.isEmpty(data))
            return;

        try {
            final Dao<T, Integer> dao = databaseHelper.getDao(clazz);
            dao.callBatchTasks(new Callable<Void>() {
                @Override
                public Void call() throws Exception {
                    for (T item : data) {
                        dao.createOrUpdate(item);
                    }
                    return null;
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // ////////////////////////////////////////////////////////////////////////////////////////////
    // EXTRACTING METHODS
    // ////////////////////////////////////////////////////////////////////////////////////////////

    public <T, E> T getForId(E id, Class<T> clazz) {
        try {
            Dao<T, E> dao = databaseHelper.getDao(clazz);
            return dao.queryForId(id);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public <T> List<T> getList(Class<T> clazz) {
        List<T> result;
        try {
            Dao<T, ?> dao = databaseHelper.getDao(clazz);
            result = dao.queryForAll();
        } catch (Exception e) {
            return Collections.emptyList();
        }
        return result;
    }

    public <T> List<String> getListField(Class<T> clazz, String fieldName) {
        List<String> result = new ArrayList<String>();
        try {
            Dao<T, ?> dao = databaseHelper.getDao(clazz);
            GenericRawResults<String[]> raws = dao.queryRaw("SELECT DISTINCT " + fieldName
                    + " FROM " + clazz.getSimpleName());

            for (String[] strings : raws)
                result.add(strings[0]);

        } catch (Exception e) {
            return Collections.emptyList();
        }
        return result;
    }

    // ////////////////////////////////////////////////////////////////////////////////////////////
    // CLEARING METHODS
    // ////////////////////////////////////////////////////////////////////////////////////////////

    public <T> void removeObjectsByColumn(final String columnName, final Object value,
                                          Class<T> clazz) {
        if (columnName == null)
            return;
        try {
            final Dao<T, String> dao = databaseHelper.getDao(clazz);
            dao.callBatchTasks(new Callable<Void>() {
                @Override
                public Void call() throws Exception {
                    DeleteBuilder<T, String> deleteBuilder = dao.deleteBuilder();
                    deleteBuilder.where().eq(columnName, value);
                    deleteBuilder.delete();
                    return null;
                }
            });
        } catch (Exception e) {
        }
    }

    public <T> void clearTable(Class<T> clazz) {
        try {
            TableUtils.clearTable(databaseHelper.getConnectionSource(), clazz);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
