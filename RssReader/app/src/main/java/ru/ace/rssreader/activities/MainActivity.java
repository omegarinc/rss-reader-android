package ru.ace.rssreader.activities;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import ru.ace.rssreader.R;
import ru.ace.rssreader.fragments.SettingsFragment;
import ru.ace.rssreader.managers.SharedPrefsManager;
import ru.ace.rssreader.utils.ToolBox;

//Main application activity
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {

            //Open Settings fragment if it's not open yet
            FragmentManager fragmentManager = getSupportFragmentManager();
            Fragment settingFragment = fragmentManager.findFragmentByTag(SettingsFragment.TAG);

            if (settingFragment == null) {
                fragmentManager.beginTransaction().replace(R.id.fragment_container, new SettingsFragment(),
                        SettingsFragment.TAG).addToBackStack(null).commit();
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    //Check internet availability
    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    //Check rss host is specified
    public boolean isRssHostSpecified() {
        String rssHostUrl = SharedPrefsManager.INSTANCE.getString(SharedPrefsManager.RSS_HOST,"");

        if(ToolBox.isEmpty(rssHostUrl))
            return false;
        else
            return true;

    }

    //Closing system keyboard
    public void closeSoftKeyboard() {
        View view = getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }
}
