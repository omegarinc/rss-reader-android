package ru.ace.rssreader.fragments;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import ru.ace.rssreader.R;
import ru.ace.rssreader.activities.MainActivity;
import ru.ace.rssreader.adapters.PostsListAdapter;
import ru.ace.rssreader.db.DatabaseManager;
import ru.ace.rssreader.managers.SharedPrefsManager;
import ru.ace.rssreader.rss.RssItem;
import ru.ace.rssreader.rss.RssParseHandler;

/**
 * Fragment containing rss post list
 */
public class PostsListFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    public static final String TAG = "PostsListFragment";

    private SwipeRefreshLayout swipeRefreshLayout;
    private PostsListAdapter adapter;
    private ListView rssList;
    private List<RssItem> rssItems = new ArrayList<>();

    public PostsListFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_posts_list, container, false);

        swipeRefreshLayout = (SwipeRefreshLayout) root.findViewById(R.id.f_posts_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(this);
        /**
         * Showing Swipe Refresh animation on activity create
         */
        swipeRefreshLayout.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        fillRssListByCash(); //Fill rss list by cashed data from local DB
                                    }
                                }
        );

        adapter = new PostsListAdapter(getActivity(), rssItems);

        rssList = (ListView) root.findViewById(R.id.f_posts_list_lv);
        rssList.setAdapter(adapter);
        rssList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                RssItem rssItem = (RssItem) parent.getItemAtPosition(position);

                PostPreviewFragment postPreviewFragment = new PostPreviewFragment();

                //Creating bundle data to transfer to post preview fragment
                Bundle bundle = new Bundle();
                bundle.putString(RssItem.TITLE, rssItem.getTitle());
                bundle.putString(RssItem.PUB_DATE, rssItem.getPubDate());
                bundle.putString(RssItem.AUTHOR, rssItem.getAuthor());
                bundle.putString(RssItem.DESCRIPTION, rssItem.getDescription());
                bundle.putString(RssItem.LINK, rssItem.getLink());
                bundle.putString(RssItem.BITMAP_URL, rssItem.getBitmapUrl());

                postPreviewFragment.setArguments(bundle);

                //Open post preview fragment
                getFragmentManager().beginTransaction().replace(R.id.fragment_container, postPreviewFragment,
                        PostPreviewFragment.TAG).addToBackStack(null).commit();

            }
        });
        return root;
    }


    //Call on rss list pull down
    @Override
    public void onRefresh() {
        onlineUpdateRssList();
    }


    //Online update rss list data
    public void onlineUpdateRssList() {

        //Check internet availability
        if (!((MainActivity) getActivity()).isOnline()) {
            Snackbar.make(rssList, R.string.net_is_not_available, Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            swipeRefreshLayout.setRefreshing(false);
            return;
        }

        //Check rss host is specified
        if (!((MainActivity) getActivity()).isRssHostSpecified()) {
            Snackbar.make(rssList, R.string.check_setting, Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            swipeRefreshLayout.setRefreshing(false);
            return;
        }

        new AsyncTask<Void, Void, Void>() {

            @Override
            protected void onPreExecute() {

                swipeRefreshLayout.setRefreshing(true); //Show progress bar
            }

            @Override
            protected Void doInBackground(Void... params) {

                List<RssItem> loadedRssItems = fetchRssData(); //Fetch rss data from rss host

                addNewRssItemsToDbCash(loadedRssItems); //Add to local DB new rss items

                updateSourceDataForRssItemsList(getRssItemsFromDb()); //Update source data of rss ListView

                return null;
            }

            @Override
            protected void onPostExecute(Void v) {

                refreshRssItemsList(); //Show updated rss posts list

                swipeRefreshLayout.setRefreshing(false); //Hide progressbar
            }
        }.execute();
    }

    //Fill rss ListView by cashed in local DB data
    public void fillRssListByCash() {

        new AsyncTask<Void, Void, Void>() {

            @Override
            protected void onPreExecute() {
                //Show progressbar
                swipeRefreshLayout.setRefreshing(true);
            }

            @Override
            protected Void doInBackground(Void... params) {

                List<RssItem> cashedRssItems = getRssItemsFromDb(); //Getting data from local DB

                updateSourceDataForRssItemsList(cashedRssItems); //Update source data for rss ListView

                return null;
            }

            @Override
            protected void onPostExecute(Void v) {

                refreshRssItemsList(); //Show updated rss posts list

                swipeRefreshLayout.setRefreshing(false); //Hide progressbar

            }
        }.execute();
    }


    //Fetch data from rss host
    public List<RssItem> fetchRssData() {

        try {

            SAXParserFactory saxPF = SAXParserFactory.newInstance();
            SAXParser saxP = saxPF.newSAXParser();
            XMLReader xmlR = saxP.getXMLReader();

            //Get rss host url from Shared preferences
            String rssHost = SharedPrefsManager.INSTANCE.getString(SharedPrefsManager.RSS_HOST, "");
            URL url = new URL(rssHost);

            //Parse rss stream
            RssParseHandler rssParseHandler = new RssParseHandler();
            xmlR.setContentHandler(rssParseHandler);
            xmlR.parse(new InputSource(url.openStream()));

            return rssParseHandler.getRssItems();

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    //Add new rss items to local DB
    private void addNewRssItemsToDbCash(List<RssItem> loadedRssItems) {

        if (loadedRssItems == null || loadedRssItems.isEmpty())
            return;

        List<RssItem> savedRssItems = DatabaseManager.INSTANCE.getList(RssItem.class);
        List<RssItem> newRssItems = new ArrayList<>();

        if (savedRssItems.isEmpty())
            newRssItems.addAll(loadedRssItems);

        for (RssItem item : loadedRssItems) {
            if (!savedRssItems.contains(item))
                newRssItems.add(item);
        }

        if (!newRssItems.isEmpty())
            DatabaseManager.INSTANCE.createOrUpdate(newRssItems, RssItem.class);

    }

    //Get rss item list from local DB table
    private List<RssItem> getRssItemsFromDb() {

        return DatabaseManager.INSTANCE.getList(RssItem.class);

    }

    //Update source data for rss ListView
    private void updateSourceDataForRssItemsList(List<RssItem> rssItemList) {

        rssItems.clear();
        rssItems.addAll(rssItemList);
        Collections.reverse(rssItems);

    }

    //Show new data in rss ListView
    private void refreshRssItemsList() {

        adapter.notifyDataSetChanged();

    }
}
