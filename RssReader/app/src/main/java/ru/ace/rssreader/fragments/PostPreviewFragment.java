package ru.ace.rssreader.fragments;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.InputStream;
import java.net.URL;

import ru.ace.rssreader.R;
import ru.ace.rssreader.rss.RssItem;
import ru.ace.rssreader.utils.ToolBox;

/**
 * Created by ACE on 29.01.2016.
 * <p/>
 * Post preview fragment to look detailed description of rss post
 */
public class PostPreviewFragment extends Fragment implements View.OnClickListener {

    public static final String TAG = "PostPreviewFragment";

    private ImageView imageView;
    private String title;
    private String pubDate;
    private String author;
    private String description;
    private String link;
    private String bitmapUrl;

    public PostPreviewFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_post_preview, container, false);

        TextView titleView = (TextView) root.findViewById(R.id.f_post_preview_title_tv);
        TextView pubDateView = (TextView) root.findViewById(R.id.f_post_preview_pub_date_tv);
        TextView authorView = (TextView) root.findViewById(R.id.f_post_preview_author_tv);
        imageView = (ImageView) root.findViewById(R.id.f_post_preview_image);
        TextView descriptionView = (TextView) root.findViewById(R.id.f_post_preview_description_tv);

        //Extracting bundle data to fill fragment views
        Bundle bundle = this.getArguments();
        title = bundle.getString(RssItem.TITLE);
        pubDate = bundle.getString(RssItem.PUB_DATE);
        author = bundle.getString(RssItem.AUTHOR);
        description = bundle.getString(RssItem.DESCRIPTION);
        link = bundle.getString(RssItem.LINK);
        bitmapUrl = bundle.getString(RssItem.BITMAP_URL);

        //Start post image download
        if (!ToolBox.isEmpty(bitmapUrl))
            new ImageAsyncLoader().execute();

        titleView.setText(title);
        titleView.setOnClickListener(this);

        pubDateView.setText(pubDate);
        authorView.setText(author);
        descriptionView.setText(description);

        return root;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.f_post_preview_title_tv:

                //Create bundle to transfer to post view fragment
                PostViewFragment postViewFragment = new PostViewFragment();
                Bundle bundle = new Bundle();
                bundle.putString(RssItem.LINK, link);
                postViewFragment.setArguments(bundle);

                //Open fragment with full original article
                getFragmentManager().beginTransaction().replace(R.id.fragment_container, postViewFragment,
                        PostViewFragment.TAG).addToBackStack(null).commit();
        }
    }

    //Async task to post image download
    private class ImageAsyncLoader extends AsyncTask<Void, Void, Bitmap> {
        @Override
        protected Bitmap doInBackground(Void... params) {
            try {
                URL imageUrl = new URL(bitmapUrl);
                Bitmap bitmap = BitmapFactory.decodeStream((InputStream) imageUrl.getContent());
                return bitmap;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            //Show loaded image on fragment
            if (bitmap != null)
                imageView.setImageBitmap(bitmap);
        }

    }
}
