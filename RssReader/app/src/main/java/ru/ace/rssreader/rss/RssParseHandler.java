package ru.ace.rssreader.rss;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by ACE on 30.01.2016.
 *
 * Handler to parse rss stream
 */
public class RssParseHandler extends DefaultHandler {

    private List<RssItem> rssItems;
    private RssItem rssItem;
    private boolean inItem = false;

    private StringBuilder content;

    public RssParseHandler() {
        rssItems = new ArrayList<>();
        content = new StringBuilder();
    }

    public List<RssItem> getRssItems() {

        //Revers rss items list to make earliest first in list
        Collections.reverse(rssItems);
        return rssItems;
    }

    public void startElement(String uri, String localName, String qName,
                             Attributes atts) throws SAXException {
        content = new StringBuilder();
        if (localName.equalsIgnoreCase("item")) {
            inItem = true;
            rssItem = new RssItem();
        } else if (inItem && localName.equalsIgnoreCase("enclosure")) {
            rssItem.setBitmapUrl(atts.getValue("url"));
        }
    }

    public void endElement(String uri, String localName, String qName)
            throws SAXException {

        if (localName.equalsIgnoreCase("title")) {
            if (inItem) {
                rssItem.setTitle(content.toString());
            }
        } else if (localName.equalsIgnoreCase("link")) {
            if (inItem) {
                rssItem.setLink(content.toString());
            }
        } else if (localName.equalsIgnoreCase("description")) {
            if (inItem) {
                rssItem.setDescription(content.toString());
            }
        } else if (inItem && localName.equalsIgnoreCase("guid")) {
            rssItem.setGuid(content.toString());
        } else if (inItem && localName.equalsIgnoreCase("pubDate")) {
            rssItem.setPubDate(content.toString());
        } else if (inItem && localName.equalsIgnoreCase("author")) {
            rssItem.setAuthor(content.toString());
        } else if (localName.equalsIgnoreCase("item")) {
            inItem = false;
            rssItems.add(rssItem);
        }
    }

    public void characters(char[] ch, int start, int length)
            throws SAXException {
        content.append(ch, start, length);
    }

    public void endDocument() throws SAXException {
    }
}
