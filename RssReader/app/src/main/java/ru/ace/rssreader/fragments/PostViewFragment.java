package ru.ace.rssreader.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import ru.ace.rssreader.R;
import ru.ace.rssreader.rss.RssItem;

/**
 * Created by USER on 29.01.2016.
 *
 * Original article of rss post fragment
 */
public class PostViewFragment extends Fragment {

    public static final String TAG = "PostViewFragment";

    public PostViewFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_post_view, container, false);

        //Get data from incoming bundle
        Bundle bundle = this.getArguments();
        String link = bundle.getString(RssItem.LINK);

        WebView postView = (WebView) root.findViewById(R.id.f_post_view_wv);

        //To avoid WebView to launch the default browser when open initial page
        postView.setWebViewClient(new WebViewClient());

        //Open original web page
        postView.loadUrl(link);

        return root;
    }
}
