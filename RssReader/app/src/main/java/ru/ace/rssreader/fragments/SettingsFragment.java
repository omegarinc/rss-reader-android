package ru.ace.rssreader.fragments;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.InputSource;

import java.net.URL;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import ru.ace.rssreader.R;
import ru.ace.rssreader.activities.MainActivity;
import ru.ace.rssreader.managers.SharedPrefsManager;

/**
 * Created by ACE on 29.01.2016.
 * <p/>
 * Settings application fragment
 */
public class SettingsFragment extends Fragment implements View.OnClickListener {

    public static final String TAG = "SettingsFragment";

    private RelativeLayout fragmentLayout;
    private TextView rssHostTv;

    public SettingsFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_settings, container, false);
        fragmentLayout = (RelativeLayout) root.findViewById(R.id.f_settings_layout_rl);
        rssHostTv = (TextView) root.findViewById(R.id.f_settings_rss_host_tv);
        rssHostTv.setText(SharedPrefsManager.INSTANCE.getString(SharedPrefsManager.RSS_HOST, ""));
        Button saveBtn = (Button) root.findViewById(R.id.f_settings_save_btn);
        saveBtn.setOnClickListener(this);
        return root;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.f_settings_save_btn:

                //Save entered rss host url to Shared Preferences
                String enteredRssHost = rssHostTv.getText().toString();
                SharedPrefsManager.INSTANCE.setString(SharedPrefsManager.RSS_HOST, enteredRssHost);

                //Close system keyboard
                ((MainActivity) getActivity()).closeSoftKeyboard();

                //Check entered host url for root "rss" tag
                new CheckHostForRssAsyncTask(enteredRssHost).execute();

                break;
        }
    }

    //Async task to check entered rss host url for root "rss" tag
    private class CheckHostForRssAsyncTask extends AsyncTask<Void, Void, Void> {

        private String hostUrl;
        private boolean isRss = false;

        CheckHostForRssAsyncTask(String hostUrl) {
            this.hostUrl = hostUrl;
        }

        @Override
        protected Void doInBackground(Void... params) {

            try {
                URL url = new URL(hostUrl);
                DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
                DocumentBuilder db = dbf.newDocumentBuilder();
                Document doc = db.parse(new InputSource(url.openStream()));
                doc.getDocumentElement().normalize();

                Element root = doc.getDocumentElement();
                String rootTagName = root.getTagName();

                if (rootTagName.equals("rss"))
                    isRss = true;

            } catch (Exception e) {
                e.printStackTrace();
                Log.w("", "Rss host url checking failed");
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            if (isRss)
                Snackbar.make(fragmentLayout, R.string.valid_rss_host, Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            else
                Snackbar.make(fragmentLayout, R.string.invalid_rss_host, Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
        }
    }
}
