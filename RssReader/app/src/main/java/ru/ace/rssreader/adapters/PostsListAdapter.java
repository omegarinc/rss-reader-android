package ru.ace.rssreader.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.InputStream;
import java.net.URL;
import java.util.List;

import ru.ace.rssreader.R;
import ru.ace.rssreader.db.DatabaseManager;
import ru.ace.rssreader.rss.RssItem;

/**
 * Created by ACE on 31.01.2016.
 * <p/>
 * Adapter for ListView containing all rss posts
 */
public class PostsListAdapter extends ArrayAdapter<RssItem> {

    // Rss post thumbnail size
    public static final int THUMBNAIL_WIDTH = 128;
    public static final int THUMBNAIL_HEIGHT = 96;

    private final Context context;
    private final List<RssItem> values;

    public PostsListAdapter(Context context, List<RssItem> values) {
        super(context, R.layout.row_posts_list, values);
        this.context = context;
        this.values = values;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        RssItem currentRssItem = values.get(position);

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View row = inflater.inflate(R.layout.row_posts_list, parent, false);

        RssItemHolder holder = new RssItemHolder();
        holder.postThumbnail = (ImageView) row.findViewById(R.id.row_posts_list_thumbnail);
        holder.postTitle = (TextView) row.findViewById(R.id.row_posts_list_title_tv);
        holder.postDesrciption = (TextView) row.findViewById(R.id.row_posts_list_description_tv);

        holder.postTitle.setText(currentRssItem.getTitle());
        holder.postDesrciption.setText(currentRssItem.getDescription());

        //Checking rss item from list for saved in local DB thumbnail
        Bitmap bitmap = currentRssItem.getThumbnail();
        if (bitmap != null)
            holder.postThumbnail.setImageBitmap(currentRssItem.getThumbnail());

            //Start Async image download if item doesn't have saved thumbnail and has url to download
            // original image
        else if (currentRssItem.getBitmapUrl() != null)
            new ImageDownloadAsyncTask(holder, values.get(position)).execute();

        return row;
    }

    //Update rss item in DB
    private void updateRssItemInDb(RssItem rssItem) {
        DatabaseManager.INSTANCE.createOrUpdate(rssItem, RssItem.class);
    }

    class RssItemHolder {
        ImageView postThumbnail;
        TextView postTitle;
        TextView postDesrciption;
    }

    //Async task to download image for rss item
    private class ImageDownloadAsyncTask extends AsyncTask<Void, Void, Void> {

        private RssItemHolder rssItemHolder;
        private RssItem rssItem;
        private Bitmap bitmap;

        ImageDownloadAsyncTask(RssItemHolder rssItemHolder, RssItem rssItem) {
            this.rssItemHolder = rssItemHolder;
            this.rssItem = rssItem;
        }

        @Override
        protected Void doInBackground(Void... params) {

            try {
                URL imageUrl = new URL(rssItem.getBitmapUrl());

                //Get original post image
                bitmap = BitmapFactory.decodeStream((InputStream) imageUrl.getContent());

                //Generate thumbnail from original image
                bitmap = Bitmap.createScaledBitmap(bitmap, THUMBNAIL_WIDTH, THUMBNAIL_HEIGHT, false);

                //Add thumbnail to rss item in DB
                rssItem.setThumbnail(bitmap);
                updateRssItemInDb(rssItem);

            } catch (Exception e) {
                Log.w("", "Downloading Image Failed");
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            //Add thumbnail to ListView item
            rssItemHolder.postThumbnail.setImageBitmap(rssItem.getThumbnail());
        }
    }
}

