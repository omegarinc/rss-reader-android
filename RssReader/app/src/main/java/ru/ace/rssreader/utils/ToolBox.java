package ru.ace.rssreader.utils;

import java.io.Closeable;
import java.io.IOException;
import java.util.Collection;

/**
 * Created by ACE on 31.01.2016.
 *
 * Utility class
 */
public class ToolBox {

    public static void quietlyClose(Closeable in){
        try {
            if(in != null)
                in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static boolean isEmpty(String str){
        return str == null || str.length() == 0;
    }

    public static <E> boolean isEmpty(Collection<E> collection){
        return collection == null || collection.isEmpty();
    }
}
