package ru.ace.rssreader.managers;

import android.content.Context;
import android.content.SharedPreferences;

import ru.ace.rssreader.RssReaderApp;
/*
* Manager for simple work with Shared Preferences
* */
public enum SharedPrefsManager {

    INSTANCE;

    public final static String RSS_HOST = "rssHost";

    private final static String SP_NAME = "prefPS";

    public int getInt(String key, int defValue) {
        return getSP().getInt(key, defValue);
    }

    public void setInt(String key, int value) {
        getSP().edit().putInt(key, value).commit();
    }

    public boolean getBool(String key, boolean defValue) {
        return getSP().getBoolean(key, defValue);
    }

    public void setBool(String key, boolean value) {
        getSP().edit().putBoolean(key, value).commit();
    }

    public float getFloat(String key, float defValue) {
        return getSP().getFloat(key, defValue);
    }

    public void setFloat(String key, float value) {
        getSP().edit().putFloat(key, value).commit();
    }

    public String getString(String key, String defValue) {
        return getSP().getString(key, defValue);
    }

    public void setString(String key, String value) {
        getSP().edit().putString(key, value).commit();
    }

    private SharedPreferences getSP() {
        return RssReaderApp.getAppContext().getSharedPreferences(SP_NAME, Context.MODE_PRIVATE);
    }
}
