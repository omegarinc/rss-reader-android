package ru.ace.rssreader.rss;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.ByteArrayOutputStream;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import ru.ace.rssreader.RssReaderApp;

/**
 * Created by ACE on 30.01.2016.
 *
 *  Rss item bean
 */
@DatabaseTable(tableName = "rssItem")
public class RssItem implements Serializable {

    public static final String GUID = "guid";
    public static final String TITLE = "title";
    public static final String LINK = "link";
    public static final String DESCRIPTION = "description";
    public static final String PUB_DATE = "pubDate";
    public static final String AUTHOR = "author";
    public static final String BITMAP_URL = "bitmapUrl";

    @DatabaseField(id = true)
    private String guid;

    @DatabaseField
    private String title;

    @DatabaseField
    private String link;

    @DatabaseField
    private String description;

    @DatabaseField
    private String pubDate;

    @DatabaseField
    private String author;

    @DatabaseField
    private String bitmapUrl;

    @DatabaseField(dataType = DataType.BYTE_ARRAY)
    private byte[] thumbnail;

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPubDate() {
        Date pubDate = null;
        try {

            //Parse string date
            SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss Z", Locale.ENGLISH);
            pubDate = dateFormat.parse(this.pubDate);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        //Get user date format
        java.text.DateFormat dateFormat = android.text.format.DateFormat.getDateFormat(RssReaderApp.getAppContext());

        //Get user time format
        java.text.DateFormat timeFormat = android.text.format.DateFormat.getTimeFormat(RssReaderApp.getAppContext());

        return dateFormat.format(pubDate) + " " + timeFormat.format(pubDate);
    }

    public void setPubDate(String pubDate) {
        this.pubDate = pubDate;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getBitmapUrl() {
        return bitmapUrl;
    }

    public void setBitmapUrl(String bitmapUrl) {
        this.bitmapUrl = bitmapUrl;
    }

    public Bitmap getThumbnail() {
        if (thumbnail != null)
            return BitmapFactory.decodeByteArray(thumbnail, 0, thumbnail.length);
        else
            return null;
    }

    public void setThumbnail(Bitmap bitmap) {

        //Create thumbnail from original image
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 0, outputStream);

        thumbnail = outputStream.toByteArray();
    }

    @Override
    public String toString() {
        return "RssItem{" +
                "guid=" + guid +
                ", title='" + title + '\'' +
                ", link='" + link + '\'' +
                ", description='" + description + '\'' +
                ", pubDate=" + pubDate +
                ", author='" + author + '\'' +
                ", bitmapUrl='" + bitmapUrl + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RssItem rssItem = (RssItem) o;

        if (!guid.equals(rssItem.guid)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return guid.hashCode();
    }
}
