package ru.ace.rssreader;

import android.app.Application;
import android.content.Context;

/**
 * Created by ACE on 30.01.2016.
 *
 * Application class to get context by static way
 */
public class RssReaderApp extends Application {

    private static Context context;

    public void onCreate() {
        super.onCreate();
        RssReaderApp.context = getApplicationContext();
    }

    public static Context getAppContext() {
        return RssReaderApp.context;
    }
}
